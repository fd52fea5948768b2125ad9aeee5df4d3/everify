$(document).ready(function(){
    $('select').material_select();
    
    $(".reportBtn").click(function() {
        window.location = "report.php";
    });

	  function refreshButtons() {
            $.ajax({
        type: 'POST',
        data: {
            'mode': 'get'
        },
        url: 'flag.helper.elab.php',
        dataType: 'json',
        success: function(phpdata) {
            if(phpdata['error'] == 1) {
                console.log(JSON.stringify(phpdata));
                window.alert("Error: Contact Admin");
            }
            if(phpdata['COPY_CONTROL'] == '1') {
                $('#copyControlBtn').html('CODE COPY: ENABLED');
                $("#copyControlBtn").addClass("green");
            }
            else if(phpdata['COPY_CONTROL'] == '0') {
                $('#copyControlBtn').html('CODE COPY: DISABLED');
                $("#copyControlBtn").removeClass();
                $("#copyControlBtn").addClass("btn-large red");
            }

            if(phpdata['FACULTY_REGISTER'] == '1') {
                $('#facultyRegisterControlBtn').html('FACULTY REGISTRATION: ENABLED');
                $("#facultyRegisterControlBtn").addClass("green");
            }
            else if(phpdata['FACULTY_REGISTER'] == '0') {
                $('#facultyRegisterControlBtn').html('FACULTY REGISTRATION: DISABLED');
                $("#facultyRegisterControlBtn").removeClass();
                $("#facultyRegisterControlBtn").addClass("btn-large red");
            }

            if(phpdata['STUDENT_REGISTER'] == '1') {
                $('#stdentRegisterControlBtn').html('STUDENT REGISTRATION: ENABLED');
                $("#stdentRegisterControlBtn").addClass("green");
            }
            else if(phpdata['STUDENT_REGISTER'] == '0') {
                $('#stdentRegisterControlBtn').html('STUDENT REGISTRATION: DISABLED');
                $("#stdentRegisterControlBtn").removeClass();
                $("#stdentRegisterControlBtn").addClass("btn-large red");
            }

        }
        });
    }


    refreshButtons();

function changeFlags(name,value) {
        $.ajax({
            type: 'POST',
            data: {
            'mode': 'set',
            'name': name,
            'val': value
            },
            url: 'flag.helper.elab.php',
            success: function(phpdata) {
                console.log(phpdata);
            if(phpdata == '1') {
                refreshButtons();
            }else {
                window.alert("ERROR CONTACT ADMIN");
            }
            }
        });
    }

    $('#copyControlBtn').click(function(){
        var value = 0;
        if($("#copyControlBtn").html() == 'CODE COPY: DISABLED') {
            value = 1;
        } else if($("#copyControlBtn").html() == 'CODE COPY: ENABLED'){
            value = 0;
        }
        changeFlags("COPY_CONTROL",value);
    });

     $('#facultyRegisterControlBtn').click(function(){
        var value = 0;
        if($("#facultyRegisterControlBtn").html() == 'FACULTY REGISTRATION: DISABLED') {
            value = 1;
        } else if($("#facultyRegisterControlBtn").html() == 'FACULTY REGISTRATION: ENABLED'){
            value = 0;
        }
        changeFlags("FACULTY_REGISTER",value);
    });

     $('#stdentRegisterControlBtn').click(function(){
        var value = 0;
        if($("#stdentRegisterControlBtn").html() == 'STUDENT REGISTRATION: DISABLED') {
            value = 1;
        } else if($("#stdentRegisterControlBtn").html() == 'STUDENT REGISTRATION: ENABLED'){
            value = 0;
        }
        changeFlags("STUDENT_REGISTER",value);
    });
});
