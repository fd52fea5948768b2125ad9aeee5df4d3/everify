<?php
session_start();
$username = $_SESSION['uname'];
$name = $_SESSION['name'];
$faculty_id = $_REQUEST['id'];
$course_name = $_SESSION['course_name'];
$course_id = $_SESSION['course_id'];
$TABLE_NAME = "users_req_table";
$TABLE_NAME_USERS = "users_table";


$faculty_data = $_REQUEST['rowData'];
$faculty_name = trim(str_replace("$faculty_id - ","",$faculty_data));


include_once(__DIR__."/../../includes/sql.config.php");

function randomPassword($len = 8){
    if(($len%2)!==0){ // Length paramenter must be a multiple of 2
        $len=8;
    }
    $length=$len-2; // Makes room for the two-digit number on the end
    $conso=array('b','c','d','f','g','h','j','k','l','m','n','p','r','s','t','v','w','x','y','z');
    $vocal=array('a','e','i','o','u');
    $password='';
    srand ((double)microtime()*1000000);
    $max = $length/2;
    for($i=1; $i<=$max; $i++){
        $password.=$conso[rand(0,19)];
        $password.=$vocal[rand(0,4)];
    }
    $password.=rand(10,99);
    $newpass = $password;
    return $newpass;
}


$sql = "SELECT * FROM `$TABLE_NAME` WHERE `UNIQUE_ID` = '$faculty_id'";
$db = mysqli_query($link,$sql);

if(!$db)
    die("Failed to Insert: ".mysqli_error($link));

if(mysqli_num_rows($db) > 0) {
    while($row = mysqli_fetch_assoc($db)) {
        $password = randomPassword();
        $studentID = $row['USER_ID'];
        // Update password in USERS TABLE
        $sql2 = "UPDATE `$TABLE_NAME` SET `PASSWORD` = '$password' WHERE `USER_ID` = '$studentID'";
        $db2 = mysqli_query($link,$sql2);
        if(!$db2)
            die("Failed to Update Student Password in USERS REQ".mysqli_error($link));

        // Update password in USERS REQ TABLE
        $password_md5 = md5($password);
        $sql2 = "UPDATE `$TABLE_NAME_USERS` SET `PASSWORD` = '$password_md5' WHERE `USER_ID` = '$studentID'";
        $db2 = mysqli_query($link,$sql2);
        if(!$db2)
            die("Failed to Update Student Password in USERS RECORD".mysqli_error($link));

    }
}

echo "Password Successfully Updated";
?>